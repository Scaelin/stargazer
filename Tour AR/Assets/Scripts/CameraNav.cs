﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraNav : MonoBehaviour
{
    public Quaternion fingerRotate;
    public Quaternion gyroscopeRotate;

    // Start is called before the first frame update
    void Start()
    {
        fingerRotate = new Quaternion(0, 0, 0, 0);
        if (SystemInfo.supportsGyroscope)
        {
            gyroscopeRotate = GyroToUnity(Input.gyro.attitude);
        }
        else
        {
            gyroscopeRotate = new Quaternion(0, 0, 0, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Change the rotate each frame

            //Finger
        




        //Rotation result
        transform.rotation = fingerRotate * gyroscopeRotate;
    }

    public Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, q.z, q.w);
    }
}
